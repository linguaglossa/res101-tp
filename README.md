# RES101 - TP 

## Getting started

This repo contains the labs for the RES101 practicals at Telecom Paris. 

## Objectives

- Understand the TCP/IP protocol stack
- Perform some experiments on real(istic) devices
- Understand the components of network systems


## Lab description

TP1: IP and ARP
TP2: TCP and UDP

For up-to-date descriptions of the lab, a shared link can be found at the current [link](https://partage.imt.fr/index.php/s/bRG2brHN6Jsqe2e).

